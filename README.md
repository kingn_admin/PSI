# 关于PSI

PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。

# PSI演示

PSI的演示见：<a target="_blank" href="https://psi.butterfly.mopaasapp.com/">https://psi.butterfly.mopaasapp.com/</a>

PC端请用`360浏览器`或者是`谷歌浏览器`访问
 
移动端扫码访问![移动端扫码访问](PSI_Mobile_URL.png)

# PSI的开源协议

PSI的开源协议为GPL v3

# PSI相关项目

1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help

2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 技术支持

如有技术方面的问题，可以提出Issue一起讨论：https://gitee.com/crm8000/PSI/issues

# 本地开发环境
参见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/06%20%E6%9C%AC%E5%9C%B0%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83">本地开发环境</a>

# 商务合作

微信/Tel: 13842851510

PSI商业计划见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/00%20%E5%95%86%E4%B8%9A%E8%AE%A1%E5%88%92">PSI 商业计划</a>

同时提供二次开发服务：

1. 总体预算不低于人民币十万元(其中用于二次开发的费用不低于五万)。

2. 需求email到：13842851510@qq.com 后，回复具体报价。
